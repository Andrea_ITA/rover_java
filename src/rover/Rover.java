package rover;

import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.TimeUnit;

public class Rover {

	public static void main(String[] args) throws IOException, InterruptedException{
		ServerSocket serverSocket = null;
		
        try {
        	System.out.println("Server started");
            serverSocket = new ServerSocket(10002);
        } catch (IOException e) {
            System.err.println("Could not listen on port: 10002");
            System.exit(1);
        }

        Socket clientSocket = null;
        try {
            clientSocket = serverSocket.accept();
        } catch (IOException e) {
            System.err.println("Accept failed.");
            System.exit(1);
        }
        PrintStream os = new PrintStream(clientSocket.getOutputStream());

        
        System.out.println("Connected. Push any key to start...");

        // enter in IDLE state
        os.println("I");
        // enter in ROVER state
        os.println("R");
        // enter in ARM state
        // os.println("A");
        

        os.close();
        
        clientSocket.close();
        serverSocket.close();
		
	}
	
	
	public String keyPressed (KeyEvent e) {
        int c = e.getKeyCode ();
        if (c==KeyEvent.VK_UP) {
        	System.out.println("UP");
            return "#60#0*"; 
        } else if(c==KeyEvent.VK_DOWN) {
        	System.out.println("DOWN");
        	return "#-60#0*"; 
        } else if(c==KeyEvent.VK_LEFT) {
        	System.out.println("LEFT");
        	return "#0#-60*";   
        } else if(c==KeyEvent.VK_RIGHT) {
        	System.out.println("RIGHT");
        	return "#0#60*";   
        } 
        
        return "I";
    }
}
