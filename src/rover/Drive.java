package rover;

import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;



public class Drive {
	private static final String UP = "#60#0*";
	//private static final String DOWN = "#-60#0*";
	private static final String LEFT = "#0#-60*";
	private static final String RIGHT = "#0#60*";
	private static final String STOP = "#0#0*";
	private final static String QUEUE_NAME = "hello";
	private final static String EXCHANGE_NAME = "obstacle";

	static PrintStream os;
	public static AtomicReference<String> position = new AtomicReference<>();
	public static AtomicReference<String> obstacle = new AtomicReference<>();
	
	public static void main(String[] args) throws IOException, InterruptedException {
		
		/* Create a new thread to retrieve information from RabbitMQ (compass data) */
		new Thread(new Runnable(){
					@Override
					public void run() {
						getCompassData();
					}
				}
		).start();
		
		/* Create a new thread to retrieve information from RabbitMQ (obstacle detection) */
		new Thread(new Runnable(){
					@Override
					public void run() {
						getObstacleData();
					}
				}
		).start();
		
		/* Start server */
        ServerSocket serverSocket = null;	
        try {
        	System.out.println("Server started");
            serverSocket = new ServerSocket(10002);
        } catch (IOException e) {
            System.err.println("Could not listen on port: 10002");
            System.exit(1);
        }

        Socket clientSocket = null;
        try {
            clientSocket = serverSocket.accept();
        } catch (IOException e) {
            System.err.println("Accept failed.");
            System.exit(1);
        }
        os = new PrintStream(clientSocket.getOutputStream());
        
        System.err.println("Connected");

        /* enter in IDLE state */
        idle();
        /* enter in ROVER state */
        rover();
        
        
        /** Task per test **/
        up();
        TimeUnit.SECONDS.sleep(4);
        left();
        TimeUnit.SECONDS.sleep(1);
        up();
        TimeUnit.SECONDS.sleep(1);
        left();
        TimeUnit.SECONDS.sleep(1);
        up();
        TimeUnit.SECONDS.sleep(4);
        
        idle();
        os.close();
        
        clientSocket.close();
        serverSocket.close();
        System.exit(0);
	}
	
	private static void getCompassData() {
		
		/* Start RabbitMQ configuration */
		ConnectionFactory factory = new ConnectionFactory();
	    factory.setHost("localhost");
	    Connection connection;
		Channel channel;
	    try {
			connection = factory.newConnection();
		    channel = connection.createChannel();
		    channel.queueDeclare(QUEUE_NAME, false, false, false, null);
			
		    /* Create consumer to get data */
			Consumer consumer = new DefaultConsumer(channel) {
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
					position.set(new String(body, "UTF-8"));
					System.out.println(" [x] Received '" + position.get() + "'");
				}
			};
		    
		    channel.basicConsume(QUEUE_NAME, true, consumer);
		} catch (IOException | TimeoutException e) {
			e.printStackTrace();
		}

	}
	
	private static void getObstacleData(){
		try{
		    ConnectionFactory factory = new ConnectionFactory();
		    factory.setHost("localhost");
		    Connection connection = factory.newConnection();
		    Channel channel = connection.createChannel();

		    channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
		    String queueName = channel.queueDeclare().getQueue();
		    channel.queueBind(queueName, EXCHANGE_NAME, "");

		    Consumer consumer = new DefaultConsumer(channel) {
		      @Override
		      public void handleDelivery(String consumerTag, Envelope envelope,
		                                 AMQP.BasicProperties properties, byte[] body) throws IOException {
		        String message = new String(body, "UTF-8");
		        System.err.println(" [x] Received '" + message + "'");
		      }
		    };
		    channel.basicConsume(queueName, true, consumer);
		} catch (IOException | TimeoutException e) {
			e.printStackTrace();
		}

	}

	static void up(){
		os.println(STOP);
        os.println(UP);
        os.flush();
        System.out.println("Up");
	}
	
	static void left(){
		os.println(STOP);
    	os.println(LEFT);
    	os.flush();
        System.out.println("Turn left");
	}
	
	static void right(){
		os.println(STOP);
    	os.println(RIGHT);
    	os.flush();
        System.out.println("Turn right");
	}
	
	static void idle(){
		os.println("I");
		os.flush();
		System.out.println("--> IDLE MODE");
	}
	
	static void rover(){
		os.println("R");
		os.flush();
		System.out.println("--> ROVER MODE");
	}
	
	static void arm(){
		os.println("A");
		os.flush();
		System.out.println("--> ARM MODE");
	}
	
}
