package rover;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.*;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;

import javax.swing.*;



public class KeyEventRover extends JFrame implements KeyListener, ActionListener {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String 	APP_NAME = "Rover manual drive";
	private static final String UP = "#60#0*";
	private static final String DOWN = "#-60#0*";
	private static final String LEFT = "#0#-60*";
	private static final String RIGHT = "#0#60*";
	private static final String STOP = "#0#0*";
	
	static JTextArea displayArea;
    JTextField typingArea;
    static PrintStream os;
    static final String newline = System.getProperty("line.separator");
    static Socket clientSocket = null;
    static ServerSocket serverSocket = null;
    static LoggerWrapper logger = null;
    static final Lock lock = new ReentrantLock();
    
    
    public static void main(String[] args) throws IOException {

        /* Turn off metal's use of bold fonts */
        UIManager.put("swing.boldMetal", Boolean.FALSE);        
        
		// Starting Logger Engine...
		logger = LoggerWrapper.getInstance();
		
		lock.lock();
        //Schedule a job for event dispatch thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
        
        /* Server part */	
        try {
        	logger.DEBUG_INFO(Level.INFO, "Server started. Waiting for rover connection...");
            serverSocket = new ServerSocket(10002);
        } catch (IOException e) {
        	logger.DEBUG_INFO(Level.SEVERE, "Could not listen on port: 10002");
            System.exit(1);
        }
        
        connect();
        logger.DEBUG_INFO(Level.INFO, "Connected");

  
    }
    
    private static void connect() throws IOException{
    	try {
            clientSocket = serverSocket.accept();
        } catch (IOException e) {
        	logger.DEBUG_INFO(Level.SEVERE, "Accept failed.");
            System.exit(1);
        }
        os = new PrintStream(clientSocket.getOutputStream());
        lock.unlock();
    }
    
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    private static void createAndShowGUI() {
        //Create and set up the window.
        KeyEventRover frame = new KeyEventRover("KeyEventRover");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        
        //Set up the content pane.
        frame.addComponentsToPane();
        
        //Display the window.
        frame.pack();
        frame.setVisible(true);
        
        //Initialize rover
        roverInitialization();
    }
    
    private static void roverInitialization(){

    	lock.lock();
        displayArea.append("CONNECTED" + newline);
        // enter in IDLE state
        os.println("I");
        displayArea.append("CMD: IDLE MODE" + newline);
        // enter in ROVER state
        os.println("R");
        displayArea.append("CMD: ENTER ROVER MODE" + newline);
        // enter in ARM state
        // os.println("A");
        lock.unlock();
    }
    
    private void addComponentsToPane() {
        
        JButton button = new JButton("Disconnect");
        button.addActionListener(this);        
        
        typingArea = new JTextField(20);
        typingArea.addKeyListener(this);
        
        displayArea = new JTextArea();
        displayArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(displayArea);
        scrollPane.setPreferredSize(new Dimension(375, 125));
        
        getContentPane().add(typingArea, BorderLayout.PAGE_START);
        getContentPane().add(scrollPane, BorderLayout.CENTER);
        getContentPane().add(button, BorderLayout.PAGE_END);
        
    }
    
    public KeyEventRover(String name) {
        super(name);
    }
    
    /** Handle the key pressed event from the text field. */
    public void keyPressed(KeyEvent e) {
        int c = e.getKeyCode();
        String key = null;
        
        os.flush();
        os.println("#0#0*");
        
        switch(c){
        	case KeyEvent.VK_UP:
        		os.println(UP);
        		key = "UP";
        		break;
        	case KeyEvent.VK_DOWN:
            	os.println(DOWN);
            	key = "DOWN";
            	break;
        	case KeyEvent.VK_LEFT:
        		os.println(LEFT);
        		key = "LEFT";
            	break;
        	case KeyEvent.VK_RIGHT:
        		os.println(RIGHT);
        		key = "RIGHT";
            	break;
            default:
            	os.println(STOP);
            	key = "STOP";
            	
        }
        
        // write information on display
        if(key!=null){
        	displayArea.append("CMD: " + key + newline);
        	displayArea.setCaretPosition(displayArea.getDocument().getLength());
        }
        
    }
    
    
    /** Handle the button click. */
    public void actionPerformed(ActionEvent e) {
    	os.println("I");
    	os.close();
        System.out.println("Disconnected");
    	
    	//Clear the text components.
        displayArea.append("Disconnected" + newline);
    	displayArea.setCaretPosition(displayArea.getDocument().getLength());

    }


	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
}
